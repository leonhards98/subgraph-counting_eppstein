# Subgraph Counting Dynamic Algorithm

This program is designed to count size four subgraphs in dynamic setting. There is an example application provided. The program is based on the modular algorithm framework
[**Algora**](https://libalgora.gitlab.io). The theoretical description can be found [here](https://link.springer.com/chapter/10.1007/978-3-642-17458-2_12)[^1]

[^1]: Eppstein, D., Goodrich, M. T., Strash, D., and Trott, L. Extended dynamic subgraph statistics using h-index parameterized data structures. In Combinatorial Optimization and Applications (Berlin, Heidelberg, 2010), W. Wu and O. Daescu, Eds., Springer Berlin Heidelberg, pp. 128–141.

The program also makes use of the static subgraph algorithm oaqc [^2] and the hash map implementation of Malte Skarupke [^3].

[^2]: Ortmann, M., and Brandes, U. Efficient orbit-aware triad and quad census in directed and undirected graphs. Applied network science 2, 1 (2017),1–17.

[^3]: Skarupke, M. Flat hash map. https://github.com/Skarupke/flat_hash_map, 2017.


## Building
To use the algorithm, it is neccersary to first compile the Algora framework. This includes [**AlgoraCore**](https://gitlab.com/AlgoRhythmics/AlgoraCore) and [**AlgoraDyn**](https://gitlab.com/AlgoRhythmics/AlgoraDyn). This program was tested using the "develop" branch of the Algora framework. For installation, see the links above.

After this is completed, the repository can be cloned and the example program compiled. To do this, move to the directory where the other Algora components were cloned into and use the following commands:

```
$ git clone https://gitlab.com/leonhards98/subgraph-counting_eppstein
$ cd subgraph-counting_eppstein
$ ./easycompile
```

The compiled binaries can then be found in the `build/Debug` and `build/Release`
subdirectories.

## Usage
The example program shows the basic usage of the program.

After a `SubgraphCounting`-Object is created, a dynamicDiGraph from the Algora framework needs to be provided. Then, the objectives need to be set. This includes wich subgraphs to count. After the object is prepared, its member functions provide all current counts. They are not saved automatically.

The procedure described above looks as follows:

    SubCounts.setGraph(&dyGraph);
	SubCounts.setObjectives(...);
	SubCounts.prepare();

    //Apply the first operation of the dynamic graph
    dyGraph.applyNextOperation();

    //Get total paw-count
    int nrPaws = SubCounts.getNrPaws(); 

To run the program, the following command can be used:

    $ ./build/Release/AlgoraApp -f tests/test2.txt -o time.txt  -a  

This counts all (-a) available total counts of the graph tests/test2.txt (-f). 
The time measurements are written into the file time.txt (-o)
