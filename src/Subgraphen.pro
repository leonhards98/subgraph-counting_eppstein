########################################################################
# Copyright (C) 2013 - 2020 : Kathrin Hanauer                          #
#                                                                      #
# This file is part of Algora.                                         #
#                                                                      #
# Algora is free software: you can redistribute it and/or modify       #
# it under the terms of the GNU General Public License as published by #
# the Free Software Foundation, either version 3 of the License, or    #
# (at your option) any later version.                                  #
#                                                                      #
# Algora is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of       #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
# GNU General Public License for more details.                         #
#                                                                      #
# You should have received a copy of the GNU General Public License    #
# along with Algora.  If not, see <http://www.gnu.org/licenses/>.      #
#                                                                      #
# Contact information:                                                 #
#   http://algora.xaikal.org                                           #
########################################################################

QT =

CONFIG += c++17

TARGET = AlgoraApp
CONFIG -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS_APP =
QMAKE_CXXFLAGS_STATIC_LIB = # remove -fPIC

QMAKE_CXXFLAGS_DEBUG += -std=c++17 -O0

QMAKE_CXXFLAGS_RELEASE -= -O3 -O2 -O1
QMAKE_CXXFLAGS_RELEASE += -std=c++17 -DNDEBUG -flto
QMAKE_LFLAGS_RELEASE += -flto -O3

general {
  QMAKE_CXXFLAGS_RELEASE += -O2 -march=x86-64
} else {
  QMAKE_CXXFLAGS_RELEASE += -O3 -march=native -mtune=native
}

debugsymbols {
	QMAKE_CXXFLAGS_RELEASE += -fno-omit-frame-pointer -g
}

profiling {
	QMAKE_CXXFLAGS_DEBUG   += -DCOLLECT_PR_DATA
	QMAKE_CXXFLAGS_RELEASE += -DCOLLECT_PR_DATA
}

SOURCES += main.cpp\
	EpsilonTab.cpp\
	StructureCounts.cpp\
	SubgraphCounts.cpp \
	oaqc/Graph.cpp\
	oaqc/QuadCensus.cpp\
    

HEADERS += test.h\
	EpsilonTab.h\
	SubgraphAuxCounts.h\
	StructureCounts.h\
	SubgraphCounts.h \
	oaqc/Graph.h\
	oaqc/QuadCensus.h\

CONFIG(release, debug|release) {
  message("Target: Release")
# uncomment the following line if you are also using AlgoraDyn
  unix:!macx: LIBS += -L$$PWD/../../AlgoraDyn/build/Release/ -lAlgoraDyn
  unix:!macx: LIBS += -L$$PWD/../../AlgoraCore/build/Release/ -lAlgoraCore
}
CONFIG(debug, debug|release) {
  message("Target: Debug")
# uncomment the following line if you are also using AlgoraDyn
  unix:!macx: LIBS += -L$$PWD/../../AlgoraDyn/build/Debug/ -lAlgoraDyn
  unix:!macx: LIBS += -L$$PWD/../../AlgoraCore/build/Debug/ -lAlgoraCore
}

INCLUDEPATH += $$PWD/../../AlgoraCore/src
DEPENDPATH += $$PWD/../../AlgoraCore/src


unix:!macx: PRE_TARGETDEPS += $$PWD/../../AlgoraCore/build/Debug/libAlgoraCore.a

# uncomment the following lines if you are also using AlgoraDyn
INCLUDEPATH += $$PWD/../../AlgoraDyn/src
DEPENDPATH += $$PWD/../../AlgoraDyn/src

INCLUDEPATH += $$PWD/../../boost_1_77_0

unix:!macx: PRE_TARGETDEPS += $$PWD/../../AlgoraDyn/build/Debug/libAlgoraDyn.a
