#include "StructureCounts.h"
#include "flat_hash_map/bytell_hash_map.hpp"
#include "graph.dyn/dynamicdigraph.h"
#include "graph.incidencelist/incidencelistvertex.h"
#include "graph/vertex.h"
#include <boost/unordered/unordered_map_fwd.hpp>
#include <string>
#include <vector>

/**
 * @brief Counts the total number of subgraphs in the given graph
 * 
 */
class SubgraphCounts{

	public:
	Algora::DynamicDiGraph *graph;
	StructureCounts StrucCount;
	int diamond {0};
	int tPath {0};
	int paw {0};
	int fCycles {0};
	int tEdges {0};
	int trinangle {0};
	int fClique {0};
	int claws {0};
	bool objectives[8] {false};

	void tCycleArcChange(const Algora::Arc *v,void (operation) (int&,int));
	void DiamondArcChange(const Algora::Arc *v,void (operation) (int&,int));
	void tPathArcChange(const Algora::Arc *v,void (operation) (int&,int));
	void pawArcChange(const Algora::Arc *v,void (operation) (int&,int));
	void fCycleArcChange(const Algora::Arc *v,void (operation) (int&,int));
	void clawArcChange(const Algora::Arc *v,void (operation) (int&,int));
	void fCliqueArcChange(const Algora::Arc *v,void (operation) (int&,int));
	void tEdgeArcChange(const Algora::Arc *v,void (operation) (int&,int));


	/**
	 * @brief Adds to a counter.
	 * Ignores counts that are <= 0
	 * 
	 * @param counter counter to add to
	 * @param increment number that is added
	 */	
	static void add(int &counter, int increment)
	{
		if (increment <= 0)
		{
			return;
		}
		counter += increment;
	}

	/**
	 * @brief Subtracts from a counter.
	 * The counter is not allowed to be < 0
	 * 
	 * @param counter counter to add to
	 * @param increment number that is added
	 */
	static void subtract(int &counter, int decrement)
	{
		if (decrement <= 0)
		{
			return;
		}
		if (decrement >= counter)
		{
			counter = 0;
			return;
		}
		counter -= decrement;
	}

	/**
	 * @brief Increases the value of the element of the given hash map defined by the given key. If the key doesn't exist, create a new entry.
	 * 
	 * @param map hash map to delete from
	 * @param key key to the element that is reduced
	 * @param increment number by which to increase
	 */
	static void increaseOrCreate(ska::bytell_hash_map<int,int> &map, int key, int increment)
	{
		if (increment <= 0) 
			return;

		map[key] += increment;

	}
	/**
	 * @brief Reduces the value of the element of the given hash map defined by the given key. 
	 * If the decrement is larger than the value, deletes the entry.
	 * 
	 * @param map hash map to delete from
	 * @param key key to the element that is reduced
	 * @param decrement number by which to decrease
	 */
	static void reduceOrDelete(ska::bytell_hash_map<int,int> &map, int key, int decrement)
	{
		if (decrement < 1)
			return;
		auto it = map.find(key);
		if (it == map.end())
			return;
		if (it->second <= decrement)
			map.erase(it);
		else
			it->second -= decrement;
	}


	public:
	/**
	 * @brief Get the time that was needed for the last operation
	 * 
	 * @return std::chrono::duration<double> 
	 */
	std::chrono::duration<double> getCurrentDynTime()
	{
		return StrucCount.EpsTab.getCurrentDynTime();
	}
	void setObjectives(bool tCycles = true, bool tPaths = true, bool claws = true, bool paws = true, bool fCycles = true, bool diamonds = true, bool fCliques = true, bool tEdge = true);
	bool prepare();

	SubgraphCounts() : graph(nullptr) {}
	void setGraph(Algora::DynamicDiGraph *graph)
	{
		this->graph = graph;
	}
	bool hasGraph() {return graph != nullptr;};

	/**
	 * @brief Get the total number of claws.
	 * 
	 * @return int 
	 */
	int getNrClaws()
	{
		return claws;
	}
	/**
	 * @brief Get the total number of four-cliques.
	 * 
	 * @return int 
	 */
	int getNrfCliques()
	{
		return fClique;
	}
	/**
	 * @brief Get the total number of two-edges.
	 * 
	 * @return int 
	 */
	int getNrtEdges()
	{
		return tEdges;
	}
	/**
	 * @brief Get the total number of diamonds.
	 * 
	 * @return int 
	 */
	int getNrDiamonds()
	{
		return diamond;
	}
	/**
	 * @brief Get the total number of three-cycles.
	 * 
	 * @return int 
	 */
	int getNrtCycles()
	{
		return trinangle;
	}
	int getNrTPaths_extended(const Algora::Vertex *u,const  Algora::Vertex *v);
	/**
	 * @brief Get the total number of three-paths.
	 * 
	 * @return int 
	 */
	int getNrTPaths()
	{
		return tPath;
	}
	/**
	 * @brief Get the total number of paws.
	 * 
	 * @return int 
	 */
	int getNrPaws()
	{
		return paw;
	}
	int getNrfCycles_extended(const Algora::Vertex *u,const  Algora::Vertex *v);
	/**
	 * @brief Get the total number of four-cycles.
	 * 
	 * @return int 
	 */
	int getNrfClycles()
	{
		return fCycles;
	}

	StructureCounts* getStructureCount()
	{
		return &StrucCount;
	}

};
