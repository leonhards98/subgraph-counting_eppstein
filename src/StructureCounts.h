#pragma once

#include "SubgraphAuxCounts.h"
#include "EpsilonTab.h"
#include "graph.dyn/dynamicdigraph.h"
#include "graph/arc.h"
#include "graph/vertex.h"
#include "graph/vertexpair.h"
#include <boost/unordered/unordered_map_fwd.hpp>
#include <functional>
#include <tuple>
#include <utility>
#include <vector>
#include <iostream>
#include "flat_hash_map/bytell_hash_map.hpp"

/**
 * @brief Manages all auxiliary counts. 
 * 
 */
class StructureCounts : public SubgraphAuxCounts
{
	public:
	EpsilonTab EpsTab;
	std::vector<int> OnVertexUpId;
	std::vector<int> OnVertexDownId;
	std::vector<int> OnEpsArcAddId;
	std::vector<int> OnEpsArcRemoveId;

	ska::bytell_hash_map<int,int> s0;
	ska::bytell_hash_map<int,int> s1;
	ska::bytell_hash_map<std::pair<int,int>,int> s2;
	ska::bytell_hash_map<std::pair<int,int>,int> s3;
	ska::bytell_hash_map<std::pair<int,int>,int> s4;
	ska::bytell_hash_map<std::pair<int,int>,int> s5;
	ska::bytell_hash_map<std::pair<int,int>,int> s6;
	ska::bytell_hash_map<std::tuple<int,int,int>,int,boost::hash<std::tuple<int,int,int>>> s7;
	bool objectives[8];



	friend class SubgraphCounts;



	void VertexChange(const Algora::Vertex *v,void (operation1) (ska::bytell_hash_map<int,int>&,int,int),
			  void (operation2) (ska::bytell_hash_map<std::pair<int,int>,int>&,int first, int second,int),
			  void (operation3) (ska::bytell_hash_map<std::tuple<int,int,int>,int,boost::hash<std::tuple<int,int,int>>>&,int first, int second, int third,int));
	void ArcChange(const Algora::Arc *a,void (operation1) (ska::bytell_hash_map<int,int>&,int,int),
			  void (operation2) (ska::bytell_hash_map<std::pair<int,int>,int>&,int first, int second,int),
			  void (operation3) (ska::bytell_hash_map<std::tuple<int,int,int>,int,boost::hash<std::tuple<int,int,int>>>&,int first, int second, int third,int));

	public:
	bool createEpsilonTable();
	void setObjectives(bool s0 = true, bool s1 = true, bool s2 = true, bool s3 = true, bool s4 = true, bool s5 = true, bool s6 = true, bool s7 = true);
	bool prepare();
	void cleanMaps();
	void cleanup();


	/**
	 * @brief Get the number of s0 structures for the given anchor vertices.
	 * 
	 * @param v anchor vertex
	 * @return int 
	 */
	int getNrs0(const Algora::Vertex *v)
	{
		auto it = s0.find(v->getId());
		return (it == s0.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s0 and their counts.
	 * 
	 * @return auto 
	 */
	auto gets0begin() const
	{
		return s0.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s0 and their counts.
	 * 
	 * @return auto 
	 */
	auto gets0end() const
	{
		return s0.end();
	}

	/**
	 * @brief Get the number of s1 structures for the given anchor vertices.
	 * 
	 * @param v anchor vertex
	 * @return int 
	 */
	int getNrs1(const Algora::Vertex *v)
	{
		auto it = s1.find(v->getId());
		return (it == s1.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s1 and their counts.
	 * 
	 * @return auto 
	 */
	auto gets1begin() const
	{
		return s1.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s1 and their counts.
	 * 
	 * @return auto 
	 */
	auto gets1end() const
	{
		return s1.end();
	}

	/**
	 * @brief Get the number of s2 structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNrs2(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = s2.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = s2.find({id1,id2});
		else
			it = s2.find({id2,id1});

		return (it == s2.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s2 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets2begin() const
	{
		return s2.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s2 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets2end() const
	{
		return s2.end();
	}

	
	/**
	 * @brief Get the number of s3 structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNrs3(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = s3.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = s3.find({id1,id2});
		else
			it = s3.find({id2,id1});

		return (it == s3.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s3 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets3begin() const
	{
		return s3.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s3 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets3end() const
	{
		return s3.end();
	}

	
	/**
	 * @brief Get the number of s4 structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNrs4(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = s4.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = s4.find({id1,id2});
		else
			it = s4.find({id2,id1});

		return (it == s4.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s4 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets4begin() const
	{
		return s4.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s4 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets4end() const
	{
		return s4.end();
	}


	/**
	 * @brief Get the number of s5 structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNrs5(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = s5.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = s5.find({id1,id2});
		else
			it = s5.find({id2,id1});

		return (it == s5.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s5 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets5begin() const
	{
		return s5.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s5 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets5end() const
	{
		return s5.end();
	}


	/**
	 * @brief Get the number of s6 structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNrs6(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = s6.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = s6.find({id1,id2});
		else
			it = s6.find({id2,id1});

		return (it == s6.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s6 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets6begin() const
	{
		return s6.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s6 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets6end() const
	{
		return s6.end();
	}


	
	/**
	 * @brief Get the number of s7 structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @param v3 third anchor vertex
	 * @return int 
	 */
	int getNrs7(const Algora::Vertex *v1, const Algora::Vertex *v2, const Algora::Vertex *v3)
	{
		auto it = s7.end();
		int id1 = v1->getId();
		int id2 = v2->getId();
		int id3 = v3->getId();

		if (id1 <= id2 && id2 <= id3)
			it = s7.find({id1,id2, id3});
		else if (id1 <= id3 && id3 <= id2)
			it = s7.find({id1,id3,id2});
		else if (id2 <= id1 && id1 <= id3)
			it = s7.find({id2,id1,id3});
		else if (id2 <= id3 && id3 <= id1)
			it = s7.find({id2,id3,id1});
		else if (id3 <= id1 && id1 <= id2)
			it = s7.find({id3,id1,id2});
		else if (id3 <= id2 && id2 <= id1)
			it = s7.find({id3,id2,id1});


		return (it == s7.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s7 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets7begin() const
	{
		return s7.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure s7 and their counts.
	 * 
	 * @return auto 
	 */
	auto  gets7end() const
	{
		return s7.end();
	}


	void printEpsTable()
	{
		EpsTab.print_table();
	}
	private:

	/**
	 * @brief Reduces the value of the element of the given hash map defined by the given key. If the decrement is larger than the value, deletes the entry.
	 * The elemnts of the key can be given in any order.
	 * 
	 * @param map hash map to delete from
	 * @param first one part of the key
	 * @param second one part of the key
	 * @param decrement number by which to decrease
	 */
	static void reduceOrDelete(ska::bytell_hash_map<std::pair<int,int>,int> &map, int first, int second, int decrement)
	{
		if (decrement < 1)
			return;

		auto it = map.end();

		if (first <= second)
			it = map.find({first,second});
		else
			it = map.find({second,first});

		if (it == map.end())
			return;
		if (it->second <= decrement)
			map.erase(it);
		else
			it->second -= decrement;
	}

	/**
	 * @brief Reduces the value of the element of the given hash map defined by the given key. If the decrement is larger than the value, deletes the entry.
	 * The elemnts of the key can be given in any order.
	 * 
	 * @param map hash map to delete from
	 * @param first one part of the key
	 * @param second one part of the key
	 * @param third one part of the key
	 * @param decrement number by which to decrease
	 */
	static void reduceOrDelete(ska::bytell_hash_map<std::tuple<int,int,int>,int,boost::hash<std::tuple<int,int,int>>> &map, int first, int second,int third, int decrement)
	{
		
		if (decrement < 1)
			return;

		auto it = map.end();

		if (first <= second && second <= third)
			it = map.find({first,second, third});
		else if (first <= third && third <= second)
			it = map.find({first,third,second});
		else if (second <= first && first <= third)
			it = map.find({second,first,third});
		else if (second <= third && third <= first)
			it = map.find({second,third,first});
		else if (third <= first && first <= second)
			it = map.find({third,first,second});
		else if (third <= second && second <= first)
			it = map.find({third,second,first});

		if (it == map.end())
			return;
		if (it->second <= decrement)
			map.erase(it);
		else
			it->second -= decrement;
	}

	/**
	 * @brief Reduces the value of the element of the given hash map defined by the given key. 
	 * If the decrement is larger than the value, deletes the entry.
	 * 
	 * @param map hash map to delete from
	 * @param key key to the element that is reduced
	 * @param decrement number by which to decrease
	 */
	static void reduceOrDelete(ska::bytell_hash_map<int,int> &map, int key, int decrement)
	{
		if (decrement < 1)
			return;
		auto it = map.find(key);
		if (it == map.end())
			return;
		if (it->second <= decrement)
			map.erase(it);
		else
			it->second -= decrement;
	}

	/**
	 * @brief Increases the value of the element of the given hash map defined by the given key. If the key doesn't exist, create a new entry.
	 * The elemnts of the key can be given in any order.
	 * 
	 * @param map hash map to delete from
	 * @param first one part of the key
	 * @param second one part of the key
	 * @param increment number by which to increase
	 */
	static void increaseOrCreate(ska::bytell_hash_map<std::pair<int,int>,int> &map, int first, int second, int increment)
	{
		if (increment <= 0) 
			return;

		if (first <= second)
			map[{first,second}] += increment;

		else
			map[{second,first}] += increment;


	}

	/**
	 * @brief Increases the value of the element of the given hash map defined by the given key. If the key doesn't exist, create a new entry.
	 * The elemnts of the key can be given in any order.
	 * 
	 * @param map hash map to delete from
	 * @param first one part of the key
	 * @param second one part of the key
	 * @param third one part of the key
	 * @param increment number by which to increase
	 */
	static void increaseOrCreate(ska::bytell_hash_map<std::tuple<int,int,int>,int,boost::hash<std::tuple<int,int,int>>> &map, int first, int second, int third, int increment)	
	{
		if (increment <= 0) 
			return;
		if (first <= second && second <= third)
			map[{first,second, third}] += increment;
		else if (first <= third && third <= second)
			map[{first,third,second}] += increment;
		else if (second <= first && first <= third)
			map[{second,first,third}] += increment;
		else if (second <= third && third <= first)
			map[{second,third,first}] += increment;
		else if (third <= first && first <= second)
			map[{third,first,second}] += increment;
		else if (third <= second && second <= first)
			map[{third,second,first}] += increment;

	}
	
	/**
	 * @brief Increases the value of the element of the given hash map defined by the given key. If the key doesn't exist, create a new entry.
	 * 
	 * @param map hash map to delete from
	 * @param key key to the element that is reduced
	 * @param increment number by which to increase
	 */
	static void increaseOrCreate(ska::bytell_hash_map<int,int> &map, int key, int increment)
	{
		if (increment <= 0) 
			return;

		map[key] += increment;

	}

};
