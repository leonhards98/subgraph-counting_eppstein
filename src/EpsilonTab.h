#pragma once

#include "SubgraphAuxCounts.h"
#include "graph.dyn/dynamicdigraph.h"
#include "graph.incidencelist/incidencelistvertex.h"
#include "graph/arc.h"
#include "graph/digraph.h"
#include "graph/graph_functional.h"
#include "graph/graphartifact.h"
#include "graph/vertex.h"
#include <boost/container_hash/extensions.hpp>
#include <boost/container_hash/hash_fwd.hpp>
#include<boost/unordered//unordered_map.hpp>
#include <functional>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <string>
#include "observable.h"
#include <iostream>
#include "flat_hash_map/bytell_hash_map.hpp"
#include "property/fastpropertymap.h"
#include <chrono>

/**
 * @brief Hahs function for std::pair<int,int> by concatenating the two 32-bit integers
 * and the using the staard hash function on the new 64-bit integer.
 * 
 */
namespace std {
template <>
struct hash<std::pair<int,int>> {
	auto operator()(const std::pair<int,int> &key) const -> size_t {
		boost::hash<size_t> hasher;
		return hasher((size_t(uint32_t(key.first)) << 32) | size_t(uint32_t(key.second)));
 
		//return (size_t(uint32_t(key.first)) << 32) | size_t(uint32_t(key.second));
		}
	};
}

class EpsilonTab : public SubgraphAuxCounts{
	double epsilon {-1};
	int nr_edges;
	int M;
	bool useSave {false};
	ska::bytell_hash_map<int,Algora::Vertex*> HighDeg;
	ska::bytell_hash_map<int,Algora::Vertex*> HighDegSave;
	ska::bytell_hash_map<int, Algora::Vertex*> B;
	std::vector<ska::bytell_hash_map<int, Algora::Vertex*>> C;
	double theta;
	bool fixed{false};

	std::function<void(bool arcAdd)> on_reompute_func = nullptr;

	Algora::Observable<Algora::Vertex*> observableVertexUp;
	Algora::Observable<Algora::Vertex*> observableVertexDown;
	Algora::Observable<Algora::Arc*> observableArcAdd;
	Algora::Observable<Algora::Arc*> observableArcRemove;

	ska::bytell_hash_map<std::pair<int,int>,bool> adjacencyMap;
	std::chrono::duration<double> currentDynTime{0};






	void activateVertex(Algora::Vertex* v, ska::bytell_hash_map<Algora::Vertex*,std::vector<Algora::Arc*>> &inactive);

	void deactivateVertex(Algora::Vertex* v,ska::bytell_hash_map<Algora::Vertex*,std::vector<Algora::Arc*>> &inactive);

	bool recompute(Algora::Arc *a, bool arcAdd);
	bool check_recompute(Algora::Arc *a, bool arcAdd);

	public:
	ska::bytell_hash_map<Algora::Arc*,int> insertedArcs;
	
	bool prepare();
	void cleanup();
	void print_table();
	void onVertexToHigh(void *id, const Algora::VertexMapping &vvFun);
	void onVertexToLow(void *id, const Algora::VertexMapping &vvFun);
	void onArcAdd(void *id, const Algora::ArcMapping &vvFun);
	void onArcRemove(void *id, const Algora::ArcMapping &vvFun);
	void removeOnVertexToLow(void *id);
	void removeOnVertexToHigh(void *id);
	void removeOnArcAdd(void *id);
	void removeOnArcRemove(void *id);
	void EpsTabOnArcRemove(Algora::Arc *a);
	void EpsTabOnArcAdd(Algora::Arc *a);
	void EpsTabOnVertexRemove(Algora::Vertex *v);
	void EpsTabOnVertexAdd(Algora::Vertex *v);
	void VertexRemove(Algora::Vertex *v, int deg);
	void VertexAdd(Algora::Vertex *v,int deg);
	void VertexUpdate(Algora::Vertex *v,int deg1, int deg2);
	/**
	 * @brief Get the time that was required for the last operations
	 * 
	 * @return std::chrono::duration<double> 
	 */
	std::chrono::duration<double> getCurrentDynTime()
	{
		return currentDynTime;
	}
	/**
	 * @brief Use the current partitioning into high and low degree vertices.
	 * 
	 */
	void useHighDeg()
	{
		useSave = false;
	}
	/**
	 * @brief Use the partitioning into high and low degree vertices that was valid at the previous step.
	 * 
	 */
	void useHighDegSave()
	{
		useSave = true;
	}
	/**
	 * @brief Fixes the tables so no vertices are moved between partitions.
	 * Also, no observers are notified when a operation in the graph takes place.
	 * 
	 */
	void fixTable()
	{
		fixed = true;
	}
	/**
	 * @brief Unfixes the tables so vertices are again moved between partitions and observers are notified
	 * 
	 */
	void unfixTable()
	{
		fixed = false;
	}
	/**
	 * @brief checks if the given vertex is high or low degree.
	 * 
	 * @param id id of the vertex to check
	 */
	bool is_high_deg(const Algora::Vertex::id_type id)
	{

		if (!hasGraph())
			throw std::runtime_error("Need to set Graph first");

		if (useSave)
			return (HighDegSave.find(id) != HighDegSave.end());
		else
			return (HighDeg.find(id) != HighDeg.end());

	}
	
	/**
	 * @brief checks if the given vertex is high or low degree.
	 * 
	 * @param id id of the vertex to check
	 */
	bool is_high_deg(const Algora::Vertex *v)
	{
		if (!hasGraph())
			throw std::runtime_error("Need to set Graph first");

		if (useSave)
			return (HighDegSave.find(v->getId()) != HighDegSave.end()); 
		else
			return (HighDeg.find(v->getId()) != HighDeg.end()); 
	}

	/**
	 * @brief checks if the given vertex is high or low degree.
	 * 
	 * @param id id of the vertex to check
	 */
	bool is_low_deg(const Algora::Vertex::id_type id)
	{
		if (!hasGraph())
			throw std::runtime_error("Need to set Graph first");

		return !is_high_deg(id);
	}
	/**
	 * @brief checks if the given vertex is high or low degree.
	 * 
	 * @param id id of the vertex to check
	 */	
	bool is_low_deg(const Algora::Vertex *v)
	{
		if (!hasGraph())
			throw std::runtime_error("Need to set Graph first");

		return !is_high_deg(v);
	}
	/**
	 * @brief Returns the iterator to the beginning of the vector that contains all high degree vertices.
	 * 
	 * @return boost::unordered_map<int, const Algora::Vertex *>::const_iterator 
	 */
	ska::bytell_hash_map<int,Algora::Vertex*>::const_iterator  getEpsbegin() const
	{
		if (useSave)
			return HighDegSave.begin();
		else
			return HighDeg.begin();
	}
	/**
	 * @brief Returns the iterator to the end of the vector that contains all high degree vertices.
	 * 
	 * @return boost::unordered_map<int, const Algora::Vertex *>::const_iterator 
	 */
	ska::bytell_hash_map<int,Algora::Vertex*>::const_iterator  getEpsend() const
	{
		if (useSave)
			return HighDegSave.end();
		else
			return HighDeg.end();
	}

	/**
	 * @brief Checks if an arc between two vertices exists using the adjacency map maintained by this class
	 * 
	 * @param v1 first vertex
	 * @param v2 second vertex
	 */
	inline bool arcExists( const Algora::Vertex  *v1, const Algora::Vertex *v2)
	{
		std::pair<int,int> key;
		if (v1->getId() < v2->getId())
		{
			key = std::pair<int,int>({v1->getId(),v2->getId()});
		}
			 
		else
		{
			key = std::pair<int,int>({v2->getId(),v1->getId()});
		}

		
		auto it = adjacencyMap.find(key);


		if ( it == adjacencyMap.end() || it->second == false)
		{
			return false;
		}
		else
		{
			return true;
		}
		

	}

	/**
	 * @brief Get the number of high degree vertices in partition 2
	 * 
	 * @return int 
	 */
	int get_eps_size()
	{
		return HighDeg.size();
	}

};

