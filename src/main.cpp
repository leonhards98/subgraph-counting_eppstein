#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <ostream>
#include <string>
#include <chrono>
#include <unistd.h>
#include <vector>
#include "graph.dyn/dynamicdigraph.h"
#include "graph/arc.h"
#include "graph/digraph.h"
#include "graph/graph_functional.h"
#include "graph/vertex.h"
#include "io/konectnetworkreader.h"
#include "CLI11.hpp"
#include "algorithm.basic.traversal/depthfirstsearch.h"
#include "oaqc/Graph.h"
#include "property/fastpropertymap.h"
#include "EpsilonTab.h"
#include "StructureCounts.h"
#include "SubgraphCounts.h"
#include "oaqc/QuadCensus.h"

using namespace Algora;

double readDynamicGraph(DynamicDiGraph &dyGraph, const std::string &filename);
bool printInfos(const DynamicDiGraph &dyGraph, const double &time);

int main(int argc, char *argv[])
{
	//Deal with all possible command line arguments and parse them
	CLI::App app{"Test"};
	std::string in_file;
	std::string statAlgo{"oaqc"};
	bool show_info{false};
	bool show_all{false};
	std::string out_file;

	bool tCycles{false};
	bool tPaths{false};
	bool claws{false};
	bool paws{false};
	bool fCycles{false};
	bool diamonds{false};
	bool fCliques{false};
	bool tEdges{false};
	bool all{false};
	bool no_stat{false};

	app.add_option("-f,--file", in_file, "Dynamic Graph Input")->required();
	app.add_flag("-i,--info", show_info, "Display Graph Infos");
	app.add_flag("-b,--debug", show_all, "Display all infos after every step");
	app.add_option("-o,--time_output", out_file, "File for time measurement outputs")->required();

	app.add_flag("-t,--triangle", tCycles, "Calcuate triangles");
	app.add_flag("-l,--tPath", tPaths, "Calcuate three paths");
	app.add_flag("-c,--claw", claws, "Calcuate claws");
	app.add_flag("-p,--paw", paws, "Calcuate paws");
	app.add_flag("-u,--fCycle", fCycles, "Calcuate four cycles");
	app.add_flag("-d,--diamond", diamonds, "Calcuate diamonds");
	app.add_flag("-x,--fClique", fCliques, "Calcuate four cliques");
	app.add_flag("-m,--tEdges", tEdges, "Calcuate two edges");
	app.add_flag("-a,--all", all, "Calcuate all subgraphs");
	app.add_flag("-n,--no_comparison", no_stat, "Don't run the static algorithm");

	CLI11_PARSE(app, argc, argv);

	if (all)
	{
		tCycles = true;
		tPaths = true;
		claws = true;
		paws = true;
		fCycles = true;
		diamonds = true;
		diamonds = true;
		fCliques = true;
	}
	DynamicDiGraph dyGraph;
	std::cout << "Reading provided dynamic Graph\n";

	double time = readDynamicGraph(dyGraph, in_file);
	if (time == -1)
		return 1;
	if (show_info)
		printInfos(dyGraph, time);


	std::ofstream outfile_time(out_file);

	std::vector<std::string> observedVertices{};

	//create a Subcounts Object and set the graph to use as well as the epsilons ans which subgraphs to count
	SubgraphCounts SubCounts;

	SubCounts.setGraph(&dyGraph);
	SubCounts.setObjectives(tCycles, tPaths, claws, paws, fCycles, diamonds, fCliques, tEdges);
	SubCounts.prepare();

	int counter = 0;
	bool done{false};

	done = dyGraph.applyNextOperation();
	std::chrono::duration<double> currentDynTime{0};
	std::chrono::duration<double> currentStatTime{0};
	std::chrono::duration<double> totalDynTime{0};

	//Loop for each operation in the graph
	while (done)
	{
		counter++;

		std::chrono::_V2::system_clock::time_point startTimeStat = std::chrono::high_resolution_clock::now();
		std::chrono::_V2::system_clock::time_point endTimeStat = std::chrono::high_resolution_clock::now();
		
		//Create a graph in the format for the static algorithm
		const unsigned long *nOrbit;
		const unsigned int *quad_mapping;
		unsigned long nCount;
		int n = dyGraph.getDiGraph()->getSize();
		int m = dyGraph.getDiGraph()->getNumArcs(1);
		int *edges_array = new int[m * 2];
		int i = 0;
		if (!no_stat)			
			dyGraph.getDiGraph()->mapArcs([&](const Algora::Arc *a)
			{
				edges_array[i] = a->getTail()->getId();
				edges_array[i+m] = a->getHead()->getId();
				i++; 
			});
		else
		{
			n = 0;
			m = 0;
		}

		//Run and time the static algorithm for the state of the algorithm before the current change
		startTimeStat = std::chrono::high_resolution_clock::now();
		auto quad = std::make_unique<oaqc::QuadCensus>(n, m, edges_array);
		nCount = quad->getNOrbitCount();
		endTimeStat = std::chrono::high_resolution_clock::now();

		nOrbit = quad->nOrbits();
		quad_mapping = quad->getMapping();

		delete[] edges_array;

		currentDynTime = SubCounts.getCurrentDynTime();
		totalDynTime += currentDynTime;
		currentStatTime = endTimeStat - startTimeStat;

		//Show debugging information
        if (show_all)
        {
		std::cout << "\n----------Step " << dyGraph.getCurrentTime() << "----------\n";
		std::cout << "\n----------Change " << counter << "----------\n\n";
		std::cout << "Time for current step of the dynamic algorithm:\t" << currentDynTime.count() << "s\n";
		std::cout << "Time for all steps of the dynamic algorithm:\t" << totalDynTime.count() << "s\n";
		std::cout << "Time for the static algorithm:\t\t\t" << currentStatTime.count() << "s\n\n";
        }

		//write the time measurements of the last operation to file.
		outfile_time << counter << " " << currentStatTime.count() << " " << currentDynTime.count() << "\n";

		if (show_all)
		{
			std::cout << "\n----Epsilon Table----\n";
			SubCounts.getStructureCount()->printEpsTable();

			std::cout << "\n----Subgraphs----\n";
			std::cout << "Triangles: " << SubCounts.getNrtCycles() << "\n";
			std::cout << "Diamonds: " << SubCounts.getNrDiamonds() << "\n";
			std::cout << "Three Paths: " << SubCounts.getNrTPaths() << "\n";
			std::cout << "Four Cycles: " << SubCounts.getNrfClycles() << "\n";
			std::cout << "Claws: " << SubCounts.getNrClaws() << "\n";
			std::cout << "Four Cliques: " << SubCounts.getNrfCliques() << "\n";
			std::cout << "Paws: " << SubCounts.getNrPaws() << "\n";
			std::cout << "Two Edge: " << SubCounts.getNrtEdges() << "\n";
		}

		//Check if the results of the dynamic algorithm.
		if (show_all)
		{
			std::cout << "\n----s0----\n";
			for (auto it = SubCounts.getStructureCount()->gets0begin(); it != SubCounts.getStructureCount()->gets0end(); it++)
				std::cout << "Vertex " << it->first << " : " << it->second << "\n";

			std::cout << "\n----s1----\n";
			for (auto it = SubCounts.getStructureCount()->gets1begin(); it != SubCounts.getStructureCount()->gets1end(); it++)
				std::cout << "Vertex " << it->first << " : " << it->second << "\n";

			std::cout << "\n----s2----\n";
			for (auto it = SubCounts.getStructureCount()->gets2begin(); it != SubCounts.getStructureCount()->gets2end(); it++)
				std::cout << "Vertex Pair " << it->first.first << "-" << it->first.second << " : " << it->second << "\n";

			std::cout << "\n----s3----\n";
			for (auto it = SubCounts.getStructureCount()->gets3begin(); it != SubCounts.getStructureCount()->gets3end(); it++)
				std::cout << "Vertex Pair " << it->first.first << "-" << it->first.second << " : " << it->second << "\n";

			std::cout << "\n----s4----\n";
			for (auto it = SubCounts.getStructureCount()->gets4begin(); it != SubCounts.getStructureCount()->gets4end(); it++)
				std::cout << "Vertex Pair " << it->first.first << "-" << it->first.second << " : " << it->second << "\n";

			std::cout << "\n----s5----\n";
			for (auto it = SubCounts.getStructureCount()->gets5begin(); it != SubCounts.getStructureCount()->gets5end(); it++)
				std::cout << "Vertex Pair " << it->first.first << "-" << it->first.second << " : " << it->second << "\n";

			std::cout << "\n----s6----\n";
			for (auto it = SubCounts.getStructureCount()->gets6begin(); it != SubCounts.getStructureCount()->gets6end(); it++)
				std::cout << "Vertex Pair " << it->first.first << "-" << it->first.second << " : " << it->second << "\n";

			std::cout << "\n----s7----\n";
			for (auto it = SubCounts.getStructureCount()->gets7begin(); it != SubCounts.getStructureCount()->gets7end(); it++)
				std::cout << "Vertex Pair " << std::get<0>(it->first) << "-" << std::get<1>(it->first) << "-" << std::get<2>(it->first) << " : " << it->second << "\n";

			std::cout << "\n";
		}


		done = dyGraph.applyNextOperation();
	}

}

/**
 * @brief Read the dynamic graph given
 * 
 * @param dyGraph graph object to write to 
 * @param filename filename of the stored graph
 * @return double time the operation took
 */
double readDynamicGraph(DynamicDiGraph &dyGraph, const std::string &filename)
{
	KonectNetworkReader reader;
	reader.setStrict(true);
	// use this version to insert all vertices at the very beginning
	// KonectNetworkReader reader(true);
	std::ifstream input(filename, std::ifstream::in);
	if (!input)
	{
		std::cerr << "Could not open input file " << filename << std::endl;
		return -1;
	}
	std::chrono::duration<double> elapsed_time;
	reader.setInputStream(&input);
	if (reader.isGraphAvailable())
	{
		auto start = std::chrono::high_resolution_clock::now();
		bool done = reader.provideDynamicDiGraph(&dyGraph);
		auto end = std::chrono::high_resolution_clock::now();
		elapsed_time = end - start;
		if (done)
		{
			auto errors = reader.getErrors();
			if (!errors.empty())
			{
				std::cerr << "Warnings: " << errors;
			}
		}
		else
		{
			std::cerr << "Errors occurred while reading graph." << std::endl;
			auto errors = reader.getErrors();
			if (!errors.empty())
			{
				std::cerr << "Errors: " << errors;
			}
		}
	}
	else
	{
		std::cerr << "No graph available. Does the file store a dynamic graph?" << std::endl;
		return -1;
	}
	return elapsed_time.count();
}

/**
 * @brief Print some basic informations about the used graph
 * 
 * @param dyGraph used graph
 * @param time Time it took to read the graph
 */
bool printInfos(const DynamicDiGraph &dyGraph, const double &time)
{
	std::cout << "\n----------Graph Informations----------\n"
			  << std::endl;
	std::cout << "Read-Time: " << time * 1000 << " seconds\n";
	std::cout << "Number of Timesteps: " << dyGraph.getMaxTime() << std::endl;
	std::cout << dyGraph.countArcAdditions(0, dyGraph.getMaxTime()) << " arcs and " << dyGraph.countVertexAdditions(0, dyGraph.getMaxTime()) << " nodes added " << std::endl;
	std::cout << dyGraph.countArcRemovals(0, dyGraph.getMaxTime()) << " arcs and " << dyGraph.countVertexRemovals(0, dyGraph.getMaxTime()) << " nodes removed " << std::endl;
	std::cout << "Final Nr of Nodes: " << dyGraph.getConstructedGraphSize() << std::endl;
	std::cout << "Final Nr of Arcs: " << dyGraph.getConstructedArcSize() << std::endl;

	return true;
}
