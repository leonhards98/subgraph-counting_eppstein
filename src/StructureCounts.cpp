#include "StructureCounts.h"
#include "graph.dyn/dynamicdigraph.h"
#include "graph/arc.h"
#include "graph/graph_functional.h"
#include "graph/vertex.h"
#include <boost/unordered/unordered_map_fwd.hpp>
#include <iostream>
#include <vector>

/**
 * @brief Create the partition that divides the vertices into low and high degree
 * using the h-index
 * 
 * @return true 
 * @return false 
 */
bool StructureCounts::createEpsilonTable()
{
	if (!hasGraph())
	{
		std::cerr << "Need to set Graph before creation Epsilon Table\n";
		return 0;
	}
	this->EpsTab = EpsilonTab();
	EpsTab.setGraph(graph);
	if (!EpsTab.prepare())
		return 0;
	return 1;

}


/**
 * @brief Set the objectives.
 * This means define which auxiliary counts need to be maintained.
 * 
 */
void StructureCounts::setObjectives(bool s0, bool s1, bool s2, bool s3, bool s4, bool s5, bool s6, bool s7)
{

	if (s0)
		objectives[0] = true;
	else
		objectives[0] = false;
	if (s1)
		objectives[1] = true;
	else
		objectives[1] = false;
	if (s2)
		objectives[2] = true;
	else
		objectives[2] = false;
	if (s3)
		objectives[3] = true;
	else
		objectives[3] = false;
	if (s4)
		objectives[4] = true;
	else
		objectives[4] = false;
	if (s5)
		objectives[5] = true;
	else
		objectives[5] = false;
	if (s6)
		objectives[6] = true;
	else
		objectives[6] = false;
	if (s7)
		objectives[7] = true;
	else
		objectives[7] = false;
}

/**
 * @brief Prepare by creating all observers of the given graph that maintain the auxiliary counts.
 * That includes whenever an arc is addded/removed and whenever a vertex changes partition
 * 
 */
bool StructureCounts::prepare()
{

	
	if (!hasGraph() || !EpsTab.hasGraph())
	{
		std::cerr << "Need to set Graph and EpsilonTable before preparing\n";
		return 0;
	}



	EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(0), [&](Algora::Arc *a) { this->ArcChange(a,increaseOrCreate,increaseOrCreate,increaseOrCreate);});
	EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(0), [&](Algora::Arc *a) { this->ArcChange(a,reduceOrDelete,reduceOrDelete,reduceOrDelete);});
	EpsTab.onVertexToHigh(&OnVertexUpId.emplace_back(0), [&](Algora::Vertex *v) { this->VertexChange(v,reduceOrDelete,reduceOrDelete,reduceOrDelete);});
	EpsTab.onVertexToLow(&OnVertexDownId.emplace_back(0), [&](Algora::Vertex *v) { this->VertexChange(v,increaseOrCreate,increaseOrCreate,increaseOrCreate);});


	return 1;
}


/**
 * @brief Clear all auxiliary counts
 * 
 */
void StructureCounts::cleanMaps()
{
	this->s0.clear();
	this->s1.clear();
	this->s2.clear();
	this->s3.clear();
	this->s4.clear();
	this->s5.clear();
	this->s6.clear();
	this->s7.clear();
		
}
/**
 * @brief Remove all added observers
 * 
 */
void StructureCounts::cleanup()
{
	cleanMaps();
	for (auto &id : OnVertexUpId)
		EpsTab.removeOnVertexToHigh(&id);
	for (auto &id : OnVertexDownId)
		EpsTab.removeOnVertexToLow(&id);
	for (auto &id : OnEpsArcAddId)
		EpsTab.removeOnArcAdd(&id);
	for (auto &id : OnEpsArcRemoveId)
		EpsTab.removeOnArcRemove(&id);

}

/**
 * @brief This function calculates the changes of all auxiliary counts when an
 * edge is inserted or deleted. It then provides this change to the given functions
 * that then increase or decrease the counts depending on if the edge was removed or inserted.
 * 
 * 
 * @param a Arc that is inserted/deleted
 * @param operation1 function that changes the count of a auxiliary structure that
 * has one anchor vertex
 * @param operation2 function that changes the count of a auxiliary structure that
 * has two anchor vertex
 * @param operation3 function that changes the count of a auxiliary structure that
 * has three anchor vertex
 */
void StructureCounts::ArcChange(const Algora::Arc *a,void (operation1) (ska::bytell_hash_map<int,int>&,int,int),
	void (operation2) (ska::bytell_hash_map<std::pair<int,int>,int>&,int first, int second,int),
	void (operation3) (ska::bytell_hash_map<std::tuple<int,int,int>,int,boost::hash<std::tuple<int,int,int>>>&,int first, int second, int third,int))
{
	auto head = a->getHead();
	auto tail = a->getTail();

	if (objectives[0] || objectives[1] || objectives[3] || objectives[4] || objectives[5] || objectives[6])
		if (EpsTab.is_low_deg(head) && EpsTab.is_low_deg(tail))
		{
			if (objectives[0] || objectives[1] || objectives[4] || objectives[5] || objectives[6])
				for (auto h = EpsTab.getEpsbegin(); h != EpsTab.getEpsend(); h++)
				{
					if (h->second == head || h->second == tail)
						continue;
					bool both_arcs {false};
					if (objectives[0] || objectives[4] || objectives[5] || objectives[6])
						if (EpsTab.arcExists(h->second,head))
						{
							operation1(s0,h->first,1);
							both_arcs = true;
							
							if (objectives[4] || objectives[5] || objectives[6])
								for (auto h2 = EpsTab.getEpsbegin(); h2 != EpsTab.getEpsend(); h2++)
								{
									if (h2 == h)
										break;
									if (h->second == head || h->second == tail)
										continue;
									if (EpsTab.arcExists(h2->second,head))
									{
										operation2(s4,h->first,h2->first,1);
										
										if (EpsTab.arcExists(h->second,tail))
											operation2(s5,h->first,h2->first,1);
										if (EpsTab.arcExists(h2->second,tail))
											operation2(s5,h->first,h2->first,1);
										

										if (EpsTab.arcExists(h->second,tail) && EpsTab.arcExists(h2->second,tail))
											operation2(s6,h->first,h2->first,1);
									}
								}
						
						}
					if (objectives[1] || objectives[0] || objectives[4] || objectives[5] || objectives[6])
						if (EpsTab.arcExists(h->second,tail))
						{
							operation1(s0,h->first,1);

							if (objectives[4] || objectives[5] || objectives[6])
								for (auto h2 = EpsTab.getEpsbegin(); h2 != EpsTab.getEpsend(); h2++)
								{
									if (h2 == h) 
										break;
									if (h->second == head || h->second == tail)
										continue;
									if (EpsTab.arcExists(h2->second,tail))
									{
										operation2(s4,h->first,h2->first,1);
										
										if (EpsTab.arcExists(h->second,head))
											operation2(s5,h->first,h2->first,1);
										if (EpsTab.arcExists(h2->second,head))
											operation2(s5,h->first,h2->first,1);
										
										if (EpsTab.arcExists(h->second,head) && EpsTab.arcExists(h2->second,head))
											operation2(s6,h->first,h2->first,1);
									}

								}
						

							if (both_arcs)
							{
								operation1(s1,h->first,1);
							}
						
						}

				}
			if (objectives[3])
			{
				graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
				{
					auto x = a1->getOther(tail);
					if (x == head)
						return;

					graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a2)
					{
						auto y = a2->getOther(head);
						if (y == tail || x == y)
							return;

						operation2(s3,x->getId(),y->getId(),1);
					});

				});

				graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
				{
					auto x = a1->getOther(tail);
					if (x == head || EpsTab.is_high_deg(x))
						return;
					graph->getDiGraph()->mapIncidentArcs(x,[&](const Algora::Arc *a2)
					{
						auto y = a2->getOther(x);
						if (y == tail || y == head)
							return;
						operation2(s3,head->getId(),y->getId(),1);
					});
				});

				graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
				{
					auto x = a1->getOther(head);
					if (x == tail || EpsTab.is_high_deg(x))
						return;
					graph->getDiGraph()->mapIncidentArcs(x,[&](const Algora::Arc *a2)
					{
						auto y = a2->getOther(x);
						if (y == head || y == tail)
							return;
						operation2(s3,tail->getId(),y->getId(),1);
					});
				});
			}




			

		}
	if (EpsTab.is_low_deg(head) && EpsTab.is_high_deg(tail))
	{
		bool done {false};
		if (objectives[0] || objectives[1] || objectives[5])
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a)
			{
				auto other = a->getOther(head);
				if (other == tail)
					return;
				done = false;
				if (objectives[5])
					graph->getDiGraph()->mapIncidentArcsUntil(head,[&](const Algora::Arc *a2)
					{
						auto y = a2->getOther(head);
						if (y == tail)
							return;
						if (!EpsTab.arcExists(other,y))
							return;
						if (EpsTab.is_high_deg(other) && EpsTab.is_low_deg(y))
							operation2(s5,tail->getId(),other->getId(),1);
						else if (EpsTab.is_high_deg(y) && EpsTab.is_low_deg(other))
							operation2(s5,tail->getId(),y->getId(),1);

					},[&done,&a](const Algora::Arc* a_now) 
					{
						if (done == false)
							done = (a == a_now);
						return done;
					});
				if (EpsTab.is_high_deg(other))
					return;
				//std::cout << "Nr 3: 1\n";
				operation1(s0,tail->getId(),1);

				if (EpsTab.arcExists(other,tail))
				{
					if (objectives[5])
						for (auto h = EpsTab.getEpsbegin(); h != EpsTab.getEpsend(); h++)
						{
							if (h->second == tail)
								continue;
							if (EpsTab.arcExists(h->second,head))
								operation2(s5,h->first,tail->getId(),1);
							if (EpsTab.arcExists(h->second,other))
								operation2(s5,h->first,tail->getId(),1);
						}

					operation1(s1,tail->getId(),1);
				}
			});
		if (objectives[2] || objectives[4] || objectives[6] || objectives[7])
			for (auto h = EpsTab.getEpsbegin(); h != EpsTab.getEpsend(); h++)
			{
				if (h->second == head || h->second == tail)
					continue;

				if (EpsTab.arcExists(h->second,head))
				{
					if (objectives[7])
						for (auto h2 = EpsTab.getEpsbegin(); h2 != EpsTab.getEpsend(); h2++)
						{
							if (h == h2)
								break;
							if (h2->second == head || h2->second == tail || !EpsTab.arcExists(h2->second,head))
								continue;
							operation3(s7,h->first,h2->first,tail->getId(),1);
						}
					//std::cout << "Nr 0: 1\n";
					operation2(s2,h->first,tail->getId(),1);

					if (objectives[4] || objectives[6])
						graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a)
						{
							auto x = a->getOther(head);
							if (x == h->second || x == tail || EpsTab.is_high_deg(x))
								return;
							operation2(s4,tail->getId(),h->first,1);
							if (EpsTab.arcExists(tail,x) && EpsTab.arcExists(h->second,x))
								operation2(s6,tail->getId(),h->first,1);
						});


				}
			}
		if (objectives[3])
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(head);
				if (x == tail || EpsTab.is_high_deg(x))
					return;
				graph->getDiGraph()->mapIncidentArcs(x,[&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(x);
					if (y == head || y == tail)
						return;
					operation2(s3,tail->getId(),y->getId(),1);
				});
			});



	}
	if (EpsTab.is_high_deg(head) && EpsTab.is_low_deg(tail))
	{
		bool done {false};
		if (objectives[0] || objectives[1] || objectives[5])
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a)
			{
				auto other = a->getOther(tail);
				if (other == head)
					return;
				done = false;
				if (objectives[5])
					graph->getDiGraph()->mapIncidentArcsUntil(tail,[&](const Algora::Arc *a2)
					{
						auto y = a2->getOther(tail);
						if (y == head)
							return;
						if (!EpsTab.arcExists(other,y))
							return;
						if (EpsTab.is_high_deg(other) && EpsTab.is_low_deg(y))
							operation2(s5,head->getId(),other->getId(),1);
						else if (EpsTab.is_high_deg(y) && EpsTab.is_low_deg(other))
							operation2(s5,head->getId(),y->getId(),1);

					},[&done,&a](const Algora::Arc* a_now) 
					{
						if (done == false)
							done = (a == a_now);
						return done;
					});
				if (EpsTab.is_high_deg(other))
					return;
				//std::cout << "Nr 4: 1\n";
				operation1(s0,head->getId(),1);

				

				if (EpsTab.arcExists(other,head))
				{
					if (objectives[5])
						for (auto h = EpsTab.getEpsbegin(); h != EpsTab.getEpsend(); h++)
						{
							if (h->second == head)
								continue;
							if (EpsTab.arcExists(h->second,tail))
								operation2(s5,h->first,head->getId(),1);
							if (EpsTab.arcExists(h->second,other))
								operation2(s5,h->first,head->getId(),1);
						}

					operation1(s1,head->getId(),1);
				}
			});
		if (objectives[2] || objectives[4] || objectives[6] || objectives[7])
			for (auto h = EpsTab.getEpsbegin(); h != EpsTab.getEpsend(); h++)
			{
				if (h->second == head || h->second == tail)
					continue;
				if (EpsTab.arcExists(h->second,tail))
				{
					if (objectives[7])
						for (auto h2 = EpsTab.getEpsbegin(); h2 != EpsTab.getEpsend(); h2++)
						{
							if (h == h2)
								break;
							if (h2->second == head || h2->second == tail || !EpsTab.arcExists(h2->second,tail))
								continue;
							operation3(s7,h->first,h2->first,head->getId(),1);
						}
					//std::cout << "Nr 0: 1\n";
					operation2(s2,h->first,head->getId(),1);

					if (objectives[4] || objectives[6])
						graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a)
						{
							auto x = a->getOther(tail);
							if (x == h->second || x == head || EpsTab.is_high_deg(x))
								return;
							operation2(s4,head->getId(),h->first,1);
							if (EpsTab.arcExists(head,x) && EpsTab.arcExists(h->second,x))
								operation2(s6,head->getId(),h->first,1);
						});
				}
			}
		if (objectives[3])
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(tail);
				if (x == head || EpsTab.is_high_deg(x))
					return;
				graph->getDiGraph()->mapIncidentArcs(x,[&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(x);
					if (y == tail || y == head)
						return;
					operation2(s3,head->getId(),y->getId(),1);
				});
			});



	}


}

/**
 * @brief This function calculates the changes of all auxiliary counts when a
 * vertex changes partition. It then provides this change to the given functions
 * that then increase or decrease the counts depending on if the vertex
 * changes from high degree to low degree or the other way around.
 * 
 * 
 * @param v Vertex that is inserted/deleted
 * @param operation1 function that changes the count of a auxiliary structure that
 * has one anchor vertex
 * @param operation2 function that changes the count of a auxiliary structure that
 * has two anchor vertex
 * @param operation3 function that changes the count of a auxiliary structure that
 * has three anchor vertex
 */
void StructureCounts::VertexChange(const Algora::Vertex *v,void (operation1) (ska::bytell_hash_map<int,int>&,int,int),
	  void (operation2) (ska::bytell_hash_map<std::pair<int,int>,int>&,int first, int second,int),
	  void (operation3) (ska::bytell_hash_map<std::tuple<int,int,int>,int,boost::hash<std::tuple<int,int,int>>>&,int first, int second, int third,int))
{
	if (objectives[0] || objectives[1] || objectives[2] || objectives[3] || objectives[4] || objectives[5] || objectives[6] || objectives[7])
	{
		std::vector<Algora::Arc*> arcs;
		EpsTab.fixTable();

		graph->getDiGraph()->mapIncidentArcs(v,[&](Algora::Arc *a)
		{
			arcs.push_back(a);
			graph->getDiGraph()->deactivateArc(a);
		});

		for (auto a : arcs)
		{
			graph->getDiGraph()->activateArc(a);
			ArcChange(a,operation1,operation2,operation3);
		}

		EpsTab.unfixTable();
	}
	
}
































