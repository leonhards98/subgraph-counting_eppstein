#include "SubgraphCounts.h"
#include "graph/arc.h"
#include "graph/vertex.h"
#include <iostream>
#include <cmath>
#include <boost/math/special_functions/binomial.hpp>
#include <boost/math/special_functions/math_fwd.hpp>

/**
 * @brief Defines which sunbgraphs should be maintained and other options for the algorithm
 *
 */
void SubgraphCounts::setObjectives(bool tCycles, bool tPaths, bool claws, bool paws, bool fCycles, bool diamonds, bool fCliques, bool tEdges)
{
	if (tCycles || paws)
		objectives[0] = true;
	else
		objectives[0] = false;
	if (tPaths)
		objectives[1] = true;
	else
		objectives[1] = false;
	if (claws)
		objectives[2] = true;
	else
		objectives[2] = false;
	if (paws)
		objectives[3] = true;
	else
		objectives[3] = false;
	if (fCycles)
		objectives[4] = true;
	else
		objectives[4] = false;
	if (diamonds)
		objectives[5] = true;
	else
		objectives[5] = false;
	if (fCliques)
		objectives[6] = true;
	else
		objectives[6] = false;
	if (tEdges)
		objectives[7] = true;
	else
		objectives[7] = false;

}

	

/**
 * @brief prepares the algorthm by intorducing all necessary observers accoring to the set options.
 * First, the observers for maintaining the auxiliary counts when an arc is removed are set.
 * Then the ones that calculate the changes in the subgraph counts.
 * Finally the ones that maintain the auxiliary counts when an arc in added are set.
 * 
 * This ensures the correct order of operations.
 */
bool SubgraphCounts::prepare()
{
	if (!hasGraph())
	{
		std::cerr << "Need to set Graph before preparing.\n";
		return 0;
	}



	//Here, the dependencies between the subgraph counts and the necessary auxiliary counts are defined.
	bool s0 {objectives[1]};
	bool s1  {objectives[3]};
	bool s2 {objectives[5] || objectives[1] || objectives[3] || objectives[6] || objectives[0]};
	bool s3 {objectives[4]};
	bool s4 {objectives[3]};
	bool s5 {objectives[5]};
	bool s6 {objectives[6]};
	bool s7  {objectives[6] || objectives[5] || objectives[3]};
	StrucCount.setGraph(graph);
	StrucCount.createEpsilonTable();
	StrucCount.setObjectives(s0,s1,s2,s3,s4,s5,s6,s7);

	// Change of aux-counts for removal
	StrucCount.EpsTab.onArcRemove(&StrucCount.OnEpsArcRemoveId.emplace_back(15), [&](Algora::Arc *a)
	{
		if (objectives[0] == true)
			tCycleArcChange(a,subtract);
		if (objectives[1] == true)
			tPathArcChange(a,subtract);
		if (objectives[2] == true)
			clawArcChange(a,subtract);
		if (objectives[3] == true)
			pawArcChange(a,subtract);
		if (objectives[4] == true)
			fCycleArcChange(a,subtract);
		if (objectives[5] == true)
			DiamondArcChange(a,subtract);
		if (objectives[6] == true)
			fCliqueArcChange(a,subtract);
		if (objectives[7] == true)
			tEdgeArcChange(a,subtract);

	});

	//Change in subgraph counts for insertion and removal
	StrucCount.prepare();


	// Change of aux-counts for insertion
	StrucCount.EpsTab.onArcAdd(&StrucCount.OnEpsArcAddId.emplace_back(15), [&](Algora::Arc *a)
	{
		if (objectives[0] == true)
			tCycleArcChange(a,add);
		if (objectives[1] == true)
			tPathArcChange(a,add);
		if (objectives[2] == true)
			clawArcChange(a,add);
		if (objectives[3] == true)
			pawArcChange(a,add);
		if (objectives[4] == true)
			fCycleArcChange(a,add);
		if (objectives[5] == true)
			DiamondArcChange(a,add);
		if (objectives[6] == true)
			fCliqueArcChange(a,add);
		if (objectives[7] == true)
			tEdgeArcChange(a,add);

	});





	return 1;
}

/**
 * @brief counts the canges in the total number of two-edges after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::tEdgeArcChange(const Algora::Arc *a,void (operation) (int&,int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();

	int increment = (graph->getDiGraph()->getNumArcs(1)-1)-(graph->getDiGraph()->getDegree(head,1)+graph->getDiGraph()->getDegree(tail,1)-2);
	operation(tEdges,increment);
	
} 

/**
 * @brief counts the canges in the total number of four-clique after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::fCliqueArcChange(const Algora::Arc *a,void (operation) (int&,int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	bool head_high {StrucCount.EpsTab.is_high_deg(head)};
	bool tail_high {StrucCount.EpsTab.is_high_deg(tail)};
	int increment {0};

	if (head_high && tail_high)
	{
		increment += StrucCount.getNrs6(head,tail);
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (h->second == head || h->second == tail)
				continue;
			
			if (StrucCount.EpsTab.arcExists(h->second,head) && StrucCount.EpsTab.arcExists(h->second,tail))
			{
				increment += StrucCount.getNrs7(head,tail,h->second);
				for (auto h2 = StrucCount.EpsTab.getEpsbegin(); h2 != StrucCount.EpsTab.getEpsend(); h2++)
				{
					if (h->second == head || h->second == tail || h2->first <= h->first)
						continue;
					
					if (StrucCount.EpsTab.arcExists(h2->second,head) && StrucCount.EpsTab.arcExists(h2->second,tail) && StrucCount.EpsTab.arcExists(h2->second,h->second))
						increment++;
				}
			}
		}



			

	}
	else
	{
		bool done {false};
		if (!head_high)
		{
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(head);
				if (x == tail || !StrucCount.EpsTab.arcExists(x,tail))
					return;
				done = false;
				graph->getDiGraph()->mapIncidentArcsUntil(head,[&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(head);
					if (y == tail)
						return;
					if (StrucCount.EpsTab.arcExists(x,y) && StrucCount.EpsTab.arcExists(y,tail))
						increment++;

				},[&done,&a1](const Algora::Arc* a_now) 
				{
					if (done == false)
						done = (a1 == a_now);
					return done;
				});
			});
		}
		else
		{
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(tail);
				if (x == head || !StrucCount.EpsTab.arcExists(x,head))
					return;
				done = false;
				graph->getDiGraph()->mapIncidentArcsUntil(tail,[&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(tail);
					if (y == head)
						return;
					if (StrucCount.EpsTab.arcExists(x,y) && StrucCount.EpsTab.arcExists(y,head))
						increment++;

				},[&done,&a1](const Algora::Arc* a_now) 
				{
					if (done == false)
						done = (a1 == a_now);
					return done;
				});
			});
		}
	}
	operation(fClique,increment);


	
} 

/**
 * @brief counts the canges in the total number of claws after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::clawArcChange(const Algora::Arc *a,void (operation) (int&,int))

{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	
	int d1 = graph->getDiGraph()->getDegree(head,1);
	int d2 = graph->getDiGraph()->getDegree(tail,1);
	int increment {0};
	if (d1 > 2)
		increment += boost::math::binomial_coefficient<double>(d1-1,2);
	if (d2 > 2)
		increment += boost::math::binomial_coefficient<double>(d2-1,2);

	operation(claws,increment);


} 

/**
 * @brief counts the canges in the total number of diamonds after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::DiamondArcChange(const Algora::Arc *a,void (operation) (int&,int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	bool head_high {StrucCount.EpsTab.is_high_deg(head)};
	bool tail_high {StrucCount.EpsTab.is_high_deg(tail)};


	int increment {0};
	if (head_high && tail_high)
	{
		int triangles = StrucCount.getNrs2(head,tail);
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (h->second == head || h->second == tail)
				continue;

			// u and v are on the outer ring
			if (StrucCount.EpsTab.arcExists(h->second,head))
				increment += StrucCount.getNrs7(head,tail,h->second);
			if (StrucCount.EpsTab.arcExists(h->second,tail))
				increment += StrucCount.getNrs7(head,tail,h->second);
			//if (StrucCount.EpsTab.arcExists(h->second,tail) && StrucCount.EpsTab.arcExists(h->second,head))
				//increment += StrucCount.getNrs7(head,tail,h->second);
			if (StrucCount.EpsTab.arcExists(h->second,head) && StrucCount.EpsTab.arcExists(h->second,tail))
			{
				increment += StrucCount.getNrs2(h->second,head);
				increment += StrucCount.getNrs2(h->second,tail);
				triangles++;
			}

		}
		triangles--;
		if (triangles > 0)
			increment += ((triangles*triangles+triangles)/2);

		increment += StrucCount.getNrs5(head,tail);
	}
	else if (!head_high && tail_high)
	{

		graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(head);
			if (x == tail)
				return;
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(head);
				if (y == tail || y == x)
					return;
				if (StrucCount.EpsTab.arcExists(y,tail) && StrucCount.EpsTab.arcExists(y,x) && (StrucCount.EpsTab.is_low_deg(x) || StrucCount.EpsTab.is_low_deg(y)))
				{
					increment++;
				}

				if (StrucCount.EpsTab.arcExists(x,tail) && StrucCount.EpsTab.arcExists(x,head) && StrucCount.EpsTab.arcExists(y,tail) && StrucCount.EpsTab.arcExists(y,head) && a1->getId() <= a2->getId())
					increment++;

			});
		});

		graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(head);
			if (x == tail || !StrucCount.EpsTab.arcExists(x,tail))
				return;

			if (StrucCount.EpsTab.is_low_deg(x))
			{
				graph->getDiGraph()->mapIncidentArcs(x,[&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(x);
					if (y == head || y == tail)
						return;
					if (StrucCount.EpsTab.arcExists(y,tail))
					{
						increment++;
					}
				});
			}
			else
			{
				increment += StrucCount.getNrs2(x,tail)-1;
			}

		});
	}
	else if (head_high && !tail_high)
	{
		graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(tail);
			if (x == head)
				return;
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(tail);
				if (y == head || y == x)
					return;
				if (StrucCount.EpsTab.arcExists(y,head) && StrucCount.EpsTab.arcExists(y,x) && (StrucCount.EpsTab.is_low_deg(x) || StrucCount.EpsTab.is_low_deg(y)))
				{
					increment++;
				}

				if (StrucCount.EpsTab.arcExists(x,tail) && StrucCount.EpsTab.arcExists(x,head) && StrucCount.EpsTab.arcExists(y,tail) && StrucCount.EpsTab.arcExists(y,head) && a1->getId() <= a2->getId())
					increment++;

			});
		});


		graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(tail);
			if (x == head || !StrucCount.EpsTab.arcExists(x,head))
				return;

			if (StrucCount.EpsTab.is_low_deg(x))
			{
				graph->getDiGraph()->mapIncidentArcs(x,[&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(x);
					if (y == head || y == tail)
						return;
					if (StrucCount.EpsTab.arcExists(y,head))
					{
						increment++;
					}
				});
			}
			else
			{
				increment += StrucCount.getNrs2(x,head)-1;
			}

		});
	}
	else if (!head_high && !tail_high)
	{
		bool done {false};
		graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(tail);
			if (x == head)
				return;
			done = false;
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(tail);
				if (y == head || y == x)
					return;
				if (StrucCount.EpsTab.arcExists(y,head) && StrucCount.EpsTab.arcExists(y,x) && (StrucCount.EpsTab.is_low_deg(x) || StrucCount.EpsTab.is_low_deg(y)))
				{
					increment++;
				}

				if (StrucCount.EpsTab.arcExists(x,tail) && StrucCount.EpsTab.arcExists(x,head) && StrucCount.EpsTab.arcExists(y,tail) && StrucCount.EpsTab.arcExists(y,head) && x->getId() <= y->getId())
					increment++;
			});
		});
		
		graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(head);
			if (x == tail)
				return;
			done = false;
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(head);
				if (y == tail)
					return;
				if (StrucCount.EpsTab.arcExists(y,tail) && StrucCount.EpsTab.arcExists(y,x) && (StrucCount.EpsTab.is_low_deg(x) || StrucCount.EpsTab.is_low_deg(y)))
				{
					increment++;
				}

			});
		});
		
	}

	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		if (h->second == tail || h->second == head)
			continue;
		if (StrucCount.EpsTab.arcExists(tail,h->second))
		{
			for (auto h2 = StrucCount.EpsTab.getEpsbegin(); h2 != StrucCount.EpsTab.getEpsend(); h2++)
			{
				if (h2->second == tail || h2->second == head || h->second == h2->second)
					continue;
				if (StrucCount.EpsTab.arcExists(h2->second,h->second) && StrucCount.EpsTab.arcExists(h2->second,tail) && StrucCount.EpsTab.arcExists(h2->second,head))
					increment++;
			}
		}
		if (StrucCount.EpsTab.arcExists(head,h->second))
		{
			for (auto h2 = StrucCount.EpsTab.getEpsbegin(); h2 != StrucCount.EpsTab.getEpsend(); h2++)
			{
				if (h2->second == tail || h2->second == head || h->second == h2->second)
					continue;
				if (StrucCount.EpsTab.arcExists(h2->second,h->second) && StrucCount.EpsTab.arcExists(h2->second,tail) && StrucCount.EpsTab.arcExists(h2->second,head))
					increment++;
			}
		}
	}
	



	operation(diamond,increment);


	
} 

/**
 * @brief counts the canges in the total number of three-paths after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::tPathArcChange(const Algora::Arc *a,void (operation) (int&,int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	int d1 = graph->getDiGraph()->getDegree(head,1);
	int d2 = graph->getDiGraph()->getDegree(tail,1);
	bool head_high {StrucCount.EpsTab.is_high_deg(head)};
	bool tail_high {StrucCount.EpsTab.is_high_deg(tail)};

    int increment = (d1-1)*(d2-1);
	if (head_high && tail_high)
	{
		increment -= StrucCount.getNrs2(head,tail);
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (StrucCount.EpsTab.arcExists(h->second,head) && StrucCount.EpsTab.arcExists(h->second,tail))
			{
				increment--;
			}
		}
	}
	else
	{
		if (!head_high)
		{
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(head);
				if (x == tail)
					return;
				if (StrucCount.EpsTab.arcExists(x,tail))
					increment--;
			});
		}
		else
		{
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(tail);
				if (x == head)
					return;
				if (StrucCount.EpsTab.arcExists(x,head))
					increment--;
			});
		}
	}


    if (head_high)
    {
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
            if (h->second == head || h->second == tail)
                continue;
            if (StrucCount.EpsTab.arcExists(h->second,head))
            {
                increment += (graph->getDiGraph()->getDegree(h->second,1)-1);
                if (StrucCount.EpsTab.arcExists(h->second,tail))
                    increment--;
            }
            
            increment += StrucCount.getNrs2(head,h->second);
            if (StrucCount.EpsTab.is_low_deg(tail) && StrucCount.EpsTab.arcExists(h->second,tail))
                increment--;
        }
		increment += StrucCount.getNrs0(head);
		if (StrucCount.EpsTab.is_low_deg(tail))
		{
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
			{
				if (StrucCount.EpsTab.is_low_deg(a1->getOther(tail)))
				{
					increment--;
					if (StrucCount.EpsTab.arcExists(a1->getOther(tail),head))
						increment--;
				}
			});
		}
        
    }
    else
    {
        graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
        {
            auto x = a1->getOther(head);
			if (x == tail)
				return;
            if (StrucCount.EpsTab.arcExists(tail,x))
                increment += graph->getDiGraph()->getDegree(x,1)-2;
            else
                increment += graph->getDiGraph()->getDegree(x,1)-1;
        });
    }
         
    if (tail_high)
    {
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
            if (h->second == head || h->second == tail)
                continue;
            if (StrucCount.EpsTab.arcExists(h->second,tail))
            {
                increment += graph->getDiGraph()->getDegree(h->second,1)-1;
                if (StrucCount.EpsTab.arcExists(h->second,head))
                    increment--;
            }

            increment += StrucCount.getNrs2(tail,h->second);
            if (StrucCount.EpsTab.is_low_deg(head) && StrucCount.EpsTab.arcExists(h->second,head))
                increment--;
        }
		increment += StrucCount.getNrs0(tail);
		if (StrucCount.EpsTab.is_low_deg(head))
		{
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
			{
				if (StrucCount.EpsTab.is_low_deg(a1->getOther(head)))
				{
					increment --;
					if (StrucCount.EpsTab.arcExists(a1->getOther(head),tail))
						increment--;
				}
			});
		}
        
    }
    else
    {
        graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
        {
            auto x = a1->getOther(tail);
			if (x == head)
				return;
            if (StrucCount.EpsTab.arcExists(head,x))
                increment += graph->getDiGraph()->getDegree(x,1)-2;
            else
                increment += graph->getDiGraph()->getDegree(x,1)-1;
        });
    }

    operation(tPath,increment);
        
}

/**
 * @brief counts the canges in the total number of paws after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::pawArcChange(const Algora::Arc *a,void (operation) (int&,int))
{
	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();
	bool head_high {StrucCount.EpsTab.is_high_deg(head)};
	bool tail_high {StrucCount.EpsTab.is_high_deg(tail)};
	int d1 = graph->getDiGraph()->getDegree(head,1);
	int d2 = graph->getDiGraph()->getDegree(tail,1);

	int increment {0};
	bool done {false};
	if (head_high)
	{
		int zw = StrucCount.getNrs1(head);
		if (zw != 0 && !tail_high)
		{
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(tail);
				if (x == head || StrucCount.EpsTab.is_high_deg(x))
					return;
				if (StrucCount.EpsTab.arcExists(x,head))
					zw--;
			});
		}
		increment += zw;
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (h->second == head || h->second == tail)
				continue;
			if (StrucCount.EpsTab.arcExists(h->second,head))
			{
				increment += StrucCount.getNrs2(head,h->second);
				if (!tail_high && StrucCount.EpsTab.arcExists(h->second,tail))
					increment--;
				for (auto h2 = StrucCount.EpsTab.getEpsbegin(); h2 != StrucCount.EpsTab.getEpsend(); h2++)
				{
					if (h == h2)
						break;
					if (h2->second == head || h2->second == tail)
						continue;
					if (StrucCount.EpsTab.arcExists(h2->second,h->second) && StrucCount.EpsTab.arcExists(h2->second,head))
						increment += 1;
				}
			}
		}
	}
	else
	{
		graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(head);
			if (x == tail)
				return;
			done = false;
			graph->getDiGraph()->mapIncidentArcsUntil(head,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(head);
				if (y == tail)
					return;
				if (StrucCount.EpsTab.arcExists(x,y))
					increment += 1;

			},[&done,&a1](const Algora::Arc* a_now)
			{
				if (done == false)
					done = (a1 == a_now);
				return done;
			});
		});
	}

	if (tail_high)
	{
		int zw = StrucCount.getNrs1(tail);
		if (zw != 0 && !head_high)
		{
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(head);
				if (x == tail || StrucCount.EpsTab.is_high_deg(x))
					return;
				if (StrucCount.EpsTab.arcExists(x,tail))
					zw--;
			});
		}
		increment += zw;
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (h->second == head || h->second == tail)
				continue;
			if (StrucCount.EpsTab.arcExists(h->second,tail))
			{
				increment += StrucCount.getNrs2(tail,h->second);
				if (!head_high && StrucCount.EpsTab.arcExists(h->second,head))
					increment--;
				for (auto h2 = StrucCount.EpsTab.getEpsbegin(); h2 != StrucCount.EpsTab.getEpsend(); h2++)
				{
					if (h2->second == head || h2->second == tail)
						continue;
					if (h == h2)
						break;
					if (StrucCount.EpsTab.arcExists(h2->second,h->second) && StrucCount.EpsTab.arcExists(h2->second,tail))
						increment += 1;
				}
			}
		}
	}
	else
	{
		graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(tail);
			if (x == head)
				return;
			done = false;
			graph->getDiGraph()->mapIncidentArcsUntil(tail,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(tail);
				if (y == head)
					return;
				if (StrucCount.EpsTab.arcExists(x,y))
					increment += 1;

			},[&done,&a1](const Algora::Arc* a_now)
			{
				if (done == false)
					done = (a1 == a_now);
				return done;
			});
		});
	}
	int nr_t {0};

	if (head_high && tail_high)
	{
		nr_t = StrucCount.getNrs2(head,tail);
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (h->second == head || h->second == tail)
				continue;
			if (StrucCount.EpsTab.arcExists(h->second,head) && StrucCount.EpsTab.arcExists(h->second,tail))
			{	
				nr_t++;
				increment += graph->getDiGraph()->getDegree(h->second,1)-2;
			}
			increment += StrucCount.getNrs7(head,tail,h->second);
		}
		increment += StrucCount.getNrs4(head,tail);
	}
	else
	{
		if (!head_high)
		{
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a)
			{
				auto x = a->getOther(head);
				if (x == tail)
					return;
				if (StrucCount.EpsTab.arcExists(x,tail))
				{
					increment += graph->getDiGraph()->getDegree(x,1)-2;
					nr_t += 1;
				}
			});
		}
		else
		{
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a)
			{
				auto x = a->getOther(tail);
				if (x == head)
					return;
				if (StrucCount.EpsTab.arcExists(x,head))
				{
					increment += graph->getDiGraph()->getDegree(x,1)-2;
					nr_t += 1;
				}
			});
		}
	}
	increment += nr_t*(d1-(2));
	increment += nr_t*(d2-(2));


			
	operation(paw,increment);
}

/**
 * @brief counts the canges in the total number of four-cycles after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::fCycleArcChange(const Algora::Arc *a,void (operation) (int&,int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();

	int increment {0};

	//std::cout << "s3: " << StrucCount.getNrs3(head,tail) << "\n";
	increment += StrucCount.getNrs3(head,tail);

	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		if (h->second == head || h->second == tail)
			continue;

		if (StrucCount.EpsTab.arcExists(h->second,head))
		{
			if (StrucCount.EpsTab.is_high_deg(tail))
			{
				increment += StrucCount.getNrs2(tail,h->second);
				if (StrucCount.EpsTab.is_low_deg(head))
					increment--;
			}
			else
			{
				graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
				{
					auto x = a1->getOther(tail);
					if (x == head || StrucCount.EpsTab.is_high_deg(x))
						return;
					if (StrucCount.EpsTab.arcExists(x,h->second))
					{
						increment++;
						//std::cout << "Nr 1: 1\n";
					}

				});
			}
			for (auto h2 = StrucCount.EpsTab.getEpsbegin(); h2 != StrucCount.EpsTab.getEpsend(); h2++)
			{
				if (h->first <= h2->first || h2->second == head || h2->second == tail)
					continue;
				if (StrucCount.EpsTab.arcExists(h2->second,tail) && StrucCount.EpsTab.arcExists(h2->second,h->second))
				{
					increment++;
					//std::cout << "Nr 2: 1\n";
				}
			}

		}
		if (StrucCount.EpsTab.arcExists(h->second,tail))
		{
			if (StrucCount.EpsTab.is_high_deg(head))
			{
				increment += StrucCount.getNrs2(head,h->second);
				if (StrucCount.EpsTab.is_low_deg(tail))
					increment--;
			}
			else
			{
				graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
				{
					auto x = a1->getOther(head);
					if (x == tail || StrucCount.EpsTab.is_high_deg(x))
						return;
					if (StrucCount.EpsTab.arcExists(x,h->second))
					{
						increment++;
						//std::cout << "Nr 3: 1\n";
					}

				});
			}
			for (auto h2 = StrucCount.EpsTab.getEpsbegin(); h2 != StrucCount.EpsTab.getEpsend(); h2++)
			{
				if (h->first <= h2->first || h2->second == head || h2->second == tail)
					continue;
				if (StrucCount.EpsTab.arcExists(h2->second,head) && StrucCount.EpsTab.arcExists(h2->second,h->second))
				{
					increment++;
						//std::cout << "Nr 4: 1\n";
				}
			}

		}
	}
	operation(fCycles,increment);

}

/**
 * @brief counts the canges in the total number of threes-cycles after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::tCycleArcChange(const Algora::Arc *a,void (operation) (int&,int))
{
	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();
	bool head_high {StrucCount.EpsTab.is_high_deg(head)};
	bool tail_high {StrucCount.EpsTab.is_high_deg(tail)};

	int increment {0};
	if (head_high && tail_high)
	{
		
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (h->second == head || h->second == tail)
				continue;
			if (StrucCount.EpsTab.arcExists(h->second,head) && StrucCount.EpsTab.arcExists(h->second,tail))
				increment++;
		}
		increment += StrucCount.getNrs2(head,tail);
	}
	else
	{
		if (!head_high)
		{
			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(head);

				if (x == tail)
					return;
				
				if (StrucCount.EpsTab.arcExists(x,tail))
					increment++;
			});
		}
        else		
		{
			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(tail);

				if (x == head)
					return;
				
				if (StrucCount.EpsTab.arcExists(x,head))
					increment++;
			});
		}
	}

	operation(trinangle,increment);
}
