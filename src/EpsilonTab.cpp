#include "EpsilonTab.h"
#include "algorithm.basic.traversal/depthfirstsearch.h"
#include "graph.dyn/dynamicdigraph.h"
#include "graph/arc.h"
#include "graph/digraph.h"
#include "graph/graph_functional.h"
#include "graph/graphartifact.h"
#include "graph/vertex.h"
#include "observable.h"
#include "property/fastpropertymap.h"
#include <cmath>
#include <iostream>
#include <ostream>
#include <stdexcept>
#include <chrono>
#include <vector>

/**
 * @brief Maintains the h-indes when a vertex gets added. Also updates the partition into high and low degree.
 * 
 * @param v vertex that is added
 * @param deg degree of vertex v
 */
void EpsilonTab::VertexAdd(Algora::Vertex *v,int deg)
{

	if ((int)C.size() <= deg)
		C.resize(deg+1);
	C.at(deg).insert({v->getId(),v});
	if (deg <= (int)HighDeg.size())
		return;
	HighDeg.insert({v->getId(),v});
	if (!B.empty())
	{
		int to_remove_id = B.begin()->first;
		auto to_remove_ptr = B.begin()->second;
		B.erase(B.begin());
		HighDeg.erase(to_remove_id);
		C.at(graph->getDiGraph()->getDegree(to_remove_ptr,0)).insert({to_remove_id,to_remove_ptr});
		if (deg == (int)HighDeg.size())
			B.insert({v->getId(),v});

	}
	else
	{
		if (!C.at(HighDeg.size()).empty())
		{
			B.clear();
			B.insert(C.at(HighDeg.size()).begin(),C.at(HighDeg.size()).end());
			C.at(HighDeg.size()).clear();
		}
		else
			B.clear();


	}

}
/**
 * @brief Maintains the h-indes when a vertex gets removed. Also updates the partition into high and low degree.
 * 
 * @param v vertex that is removed
 * @param deg degree of vertex v
 */
void EpsilonTab::VertexRemove(Algora::Vertex *v,int deg)
{
	if (C.size() <= HighDeg.size())
		C.resize(HighDeg.size()+1);
	B.erase(v->getId());
	C.at(deg).erase(v->getId());
	if (!HighDeg.erase(v->getId()))
		return;
	if (!C.at(HighDeg.size()+1).empty())
	{
		int key = C.at(HighDeg.size()+1).begin()->first;
		auto val = C.at(HighDeg.size()+1).begin()->second;
		B.insert({key,val});
		HighDeg.insert({key,val});
		C.at(HighDeg.size()).erase(key);
	}
	else
	{
		C.at(HighDeg.size()+1).insert(B.begin(),B.end());
		B.clear();
	}
	if ((C.end()-1)->empty())
		C.erase(C.end()-1);

}
/**
 * @brief Maintains the h-indes when a vertex gets added. Also updates the partition into high and low degree.
 * 
 * @param v vertex that is added
 */
void EpsilonTab::EpsTabOnVertexAdd(Algora::Vertex *v)
{
	if (fixed)
		return;
	VertexAdd(v,graph->getDiGraph()->getDegree(v,0));
}
/**
 * @brief Maintains the h-indes when a vertex gets added. Also updates the partition into high and low degree.
 * 
 * @param v vertex that is added
 */
void EpsilonTab::EpsTabOnVertexRemove(Algora::Vertex *v)
{
	if (fixed)
		return;
	VertexRemove(v,graph->getDiGraph()->getDegree(v,0));
}

/**
 * @brief Deactivates a vertex.
 * 
 * @param v vertex to deactivate
 * @param inactive hash map containing each inactive vertex with all its edges
 */
void EpsilonTab::deactivateVertex(Algora::Vertex* v,ska::bytell_hash_map<Algora::Vertex*,std::vector<Algora::Arc*>> &inactive)
{
	graph->getDiGraph()->mapIncidentArcs(v,[&](Algora::Arc* a)
	{
		inactive[v].push_back(a);
		graph->getDiGraph()->deactivateArc(a);
	});
}
/**
 * @brief Activates a vertex.
 * 
 * @param v vertex to activate
 * @param inactive hash map containing each inactive vertex with all its edges
 */
void EpsilonTab::activateVertex(Algora::Vertex* v,ska::bytell_hash_map<Algora::Vertex*,std::vector<Algora::Arc*>> &inactive)
{
	for (auto i : inactive[v])
		graph->getDiGraph()->activateArc(i);
	
	inactive[v].clear();
	
}
/**
 * @brief Maintains the h-index and partitioning into high and low degree
 * when a vertex changes degree.
 * 
 * @param v vertex whose degree changes
 * @param deg1 degree the vertex v currently has
 * @param deg2 new degree the vertex v 
 */
void EpsilonTab::VertexUpdate(Algora::Vertex *v,int deg1, int deg2)
{
	if (deg1 == (int)HighDeg.size())
	{
		if (static_cast<int>(C.size()) <= deg2)
		C.resize(deg2+1);

		B.erase(v->getId());
		C.at(deg2).insert({v->getId(),v});
	}
	else if (deg2 == (int)HighDeg.size())
	{
		C.at(deg1).erase(v->getId());
		B.insert({v->getId(),v});
	}
	else 
	{
		if (static_cast<int>(C.size()) <= deg2)
		C.resize(deg2+1);

		C.at(deg1).erase(v->getId());
		C.at(deg2).insert({v->getId(),v});
	}
}

/**
 * @brief Maintains the h-index and partitioning into high and low degree vertices
 * when a new edge gets inserted and then calls all observers.
 * The time measurements are also done here.
 * 
 * @param a Edge that gets inserted
 */
void EpsilonTab::EpsTabOnArcAdd(Algora::Arc *a)
{
	if (fixed)
		return;

	auto startTimeDyn = std::chrono::high_resolution_clock::now();
	auto head = a->getHead();
	auto tail = a->getTail();
	bool head_high = is_high_deg(head);
	bool tail_high = is_high_deg(tail);


	observableArcAdd.notifyObservers(a);	
	
	HighDegSave = HighDeg;	


	if (graph->getDiGraph()->getDegree(head,0) > 1 && !head_high)
		VertexRemove(head,graph->getDiGraph()->getDegree(head,0)-1);
	if (graph->getDiGraph()->getDegree(tail,0) > 1 && !tail_high)
		VertexRemove(tail,graph->getDiGraph()->getDegree(tail,0)-1);

	if (!head_high)
		VertexAdd(head,graph->getDiGraph()->getDegree(head,0));
	else
		VertexUpdate(head,graph->getDiGraph()->getDegree(head,0)-1, graph->getDiGraph()->getDegree(head,0));

	if (!tail_high)
		VertexAdd(tail,graph->getDiGraph()->getDegree(tail,0));
	else
		VertexUpdate(tail,graph->getDiGraph()->getDegree(tail,0)-1, graph->getDiGraph()->getDegree(tail,0));

	std::vector<Algora::Vertex*> new_high {};
	std::vector<Algora::Vertex*> new_low {};
	ska::bytell_hash_map<Algora::Vertex*,std::vector<Algora::Arc*>> inactive;

	for (auto h = HighDeg.begin(); h != HighDeg.end(); h++)
		if (HighDegSave.find(h->first) == HighDegSave.end())
			new_high.push_back(h->second);

	for (auto h = HighDegSave.begin(); h != HighDegSave.end(); h++)
		if (HighDeg.find(h->first) == HighDeg.end())
			new_low.push_back(h->second);

	this->useHighDegSave();

	if (new_high.size() > 0)
	{
		fixTable();
		for (auto h : new_low)
		{
			deactivateVertex(h,inactive);

		}
		for (auto h : new_high)
		{
			deactivateVertex(h,inactive);

		}
		unfixTable();

		for (auto h : new_high)
		{
			fixTable();
			activateVertex(h,inactive);
			unfixTable();
			observableVertexUp.notifyObservers(h);
		}
		fixTable();
		for (auto h : new_low)
		{
			activateVertex(h,inactive);
		}
		unfixTable();
	}
	if (new_low.size() > 0)
	{
		fixTable();
		for (auto h : new_low)
			deactivateVertex(h,inactive);
		unfixTable();

		for (auto h : new_low)
		{
			fixTable();
			activateVertex(h,inactive);
			unfixTable();
			observableVertexUp.notifyObservers(h);
		}

	}

	/*
	for (auto h = HighDeg.begin(); h != HighDeg.end(); h++)
		if (HighDegSave.find(h->first) == HighDegSave.end())
			observableVertexUp.notifyObservers(h->second);

	for (auto h = HighDegSave.begin(); h != HighDegSave.end(); h++)
		if (HighDeg.find(h->first) == HighDeg.end())
			observableVertexUp.notifyObservers(h->second);
	*/
	this->useHighDeg();


	if (new_high.size() > 0)
	{
		fixTable();
		for (auto h : new_low)
		{
			deactivateVertex(h,inactive);
		}
		for (auto h : new_high)
			deactivateVertex(h,inactive);

		unfixTable();

		for (auto h : new_high)
		{
			fixTable();
			activateVertex(h,inactive);
			unfixTable();
			observableVertexDown.notifyObservers(h);
		}
		fixTable();
		for (auto h : new_low)
		{
			activateVertex(h,inactive);
		}
		unfixTable();
		
	}


	if (new_low.size() > 0)
	{
		fixTable();

		for (auto h : new_low)
			deactivateVertex(h,inactive);

		unfixTable();

		for (auto h : new_low)
		{
			fixTable();
			activateVertex(h,inactive);
			unfixTable();
			observableVertexDown.notifyObservers(h);
		}

	}

	/*
	for (auto h = HighDeg.begin(); h != HighDeg.end(); h++)
		if (HighDegSave.find(h->first) == HighDegSave.end())
			observableVertexDown.notifyObservers(h->second);

	for (auto h = HighDegSave.begin(); h != HighDegSave.end(); h++)
		if (HighDeg.find(h->first) == HighDeg.end())
			observableVertexDown.notifyObservers(h->second);
	*/

	auto endTimeDyn = std::chrono::high_resolution_clock::now();
	currentDynTime = endTimeDyn - startTimeDyn;

}
/**
 * @brief Maintains the h-index and partitioning into high and low degree vertices
 * when an edge gets removed and then calls all observers.
 * The time measurements are also done here.
 * 
 * @param a Edge that gets removed
 */
void EpsilonTab::EpsTabOnArcRemove(Algora::Arc *a)
{
	if (fixed)
		return;

	auto startTimeDyn = std::chrono::high_resolution_clock::now();
	auto head = a->getHead();
	auto tail = a->getTail();
	bool head_high = is_high_deg(head);
	bool tail_high = is_high_deg(tail);
	int high_size = HighDeg.size();

	observableArcRemove.notifyObservers(a);

	HighDegSave = HighDeg;	

	if (!(static_cast<int>(graph->getDiGraph()->getDegree(head,0)-1) >= high_size && head_high))
		VertexRemove(head,graph->getDiGraph()->getDegree(head,0));
	else
		VertexUpdate(head, graph->getDiGraph()->getDegree(head,0), graph->getDiGraph()->getDegree(head,0)-1);

	if (!(static_cast<int>(graph->getDiGraph()->getDegree(tail,0)-1) >= high_size && tail_high))
		VertexRemove(tail,graph->getDiGraph()->getDegree(tail,0));
	else
		VertexUpdate(tail, graph->getDiGraph()->getDegree(tail,0), graph->getDiGraph()->getDegree(tail,0)-1);


	if (graph->getDiGraph()->getDegree(head,0) > 1 && !(static_cast<int>(graph->getDiGraph()->getDegree(head,0)-1) >= high_size && head_high))
		VertexAdd(head,graph->getDiGraph()->getDegree(head,0)-1);
	if (graph->getDiGraph()->getDegree(tail,0) > 1 && !(static_cast<int>(graph->getDiGraph()->getDegree(tail,0)-1) >= high_size && tail_high))
		VertexAdd(tail,graph->getDiGraph()->getDegree(tail,0)-1);

	std::vector<Algora::Vertex*> new_high;
	std::vector<Algora::Vertex*> new_low;
	ska::bytell_hash_map<Algora::Vertex*,std::vector<Algora::Arc*>> inactive;
	for (auto h = HighDeg.begin(); h != HighDeg.end(); h++)
		if (HighDegSave.find(h->first) == HighDegSave.end())
			new_high.push_back(h->second);

	for (auto h = HighDegSave.begin(); h != HighDegSave.end(); h++)
		if (HighDeg.find(h->first) == HighDeg.end())
			new_low.push_back(h->second);

	this->useHighDegSave();

	
	
	if (new_high.size() > 0)
	{
		fixTable();
		for (auto h : new_low)
		{
			deactivateVertex(h,inactive);

		}
		for (auto h : new_high)
		{
			deactivateVertex(h,inactive);

		}
		unfixTable();

		for (auto h : new_high)
		{
			fixTable();
			activateVertex(h,inactive);
			unfixTable();
			observableVertexUp.notifyObservers(h);
		}
		fixTable();
		for (auto h : new_low)
		{
			activateVertex(h,inactive);
		}
		unfixTable();
	}
	if (new_low.size() > 0)
	{
		fixTable();
		for (auto h : new_low)
			deactivateVertex(h,inactive);
		unfixTable();

		for (auto h : new_low)
		{
			fixTable();
			activateVertex(h,inactive);
			unfixTable();
			observableVertexUp.notifyObservers(h);
		}

	}

	/*
	for (auto h = HighDeg.begin(); h != HighDeg.end(); h++)
		if (HighDegSave.find(h->first) == HighDegSave.end())
			observableVertexUp.notifyObservers(h->second);

	for (auto h = HighDegSave.begin(); h != HighDegSave.end(); h++)
		if (HighDeg.find(h->first) == HighDeg.end())
			observableVertexUp.notifyObservers(h->second);
	*/
	this->useHighDeg();


	if (new_high.size() > 0)
	{
		fixTable();
		for (auto h : new_low)
		{
			deactivateVertex(h,inactive);
		}
		for (auto h : new_high)
			deactivateVertex(h,inactive);

		unfixTable();

		for (auto h : new_high)
		{
			fixTable();
			activateVertex(h,inactive);
			unfixTable();
			observableVertexDown.notifyObservers(h);
		}
		fixTable();
		for (auto h : new_low)
		{
			activateVertex(h,inactive);
		}
		unfixTable();
		
	}


	if (new_low.size() > 0)
	{
		fixTable();
		for (auto h : new_low)
			deactivateVertex(h,inactive);

		unfixTable();

		for (auto h : new_low)
		{
			fixTable();
			activateVertex(h,inactive);
			unfixTable();
			observableVertexDown.notifyObservers(h);
		}
	}

	/*
	for (auto h = HighDeg.begin(); h != HighDeg.end(); h++)
		if (HighDegSave.find(h->first) == HighDegSave.end())
			observableVertexDown.notifyObservers(h->second);

	for (auto h = HighDegSave.begin(); h != HighDegSave.end(); h++)
		if (HighDeg.find(h->first) == HighDeg.end())
			observableVertexDown.notifyObservers(h->second);
	*/

	auto endTimeDyn = std::chrono::high_resolution_clock::now();
	currentDynTime = endTimeDyn - startTimeDyn;
}


/**
 * @brief Prepares the object. This includes linking with the observers of the graph given.
 * Mainatins the adjacency map that contains all presently active arcs
 * 
 */
bool EpsilonTab::prepare()
{
	graph->getDiGraph()->onVertexRemove(&OnVertexRemoveId.emplace_back(0),[&](Algora::Vertex *v)
	{

		EpsTabOnVertexRemove(v);



	});
	graph->getDiGraph()->onArcAdd(&OnArcAddId.emplace_back(0), [&](Algora::Arc *a)
	{

		insertedArcs[a] = 1;
		int head = a->getHead()->getId();
		int tail = a->getTail()->getId();

		std::pair<int,int> key;
		if (head < tail)
		{
		 	key = std::pair<int,int>({head,tail});
		}
		else
		{
			key = std::pair<int,int>({tail,head});
		}


		adjacencyMap[key] = true;



		EpsTabOnArcAdd(a);

		
	});
			
	graph->getDiGraph()->onArcRemove(&OnArcRemoveId.emplace_back(0), [&](Algora::Arc *a)
	{
		insertedArcs.erase(a);



		int head = a->getHead()->getId();
		int tail = a->getTail()->getId();

		std::pair<int,int> key;
		if (head < tail)
		{
			key = std::pair<int,int>({head,tail});

		}
			 
		else
		{
			key = std::pair<int,int>({tail,head});

		}
		
		adjacencyMap[key] = false;

		EpsTabOnArcRemove(a);





	});

	return 1;

}

/**
 * @brief Resets the partitioning. All vertices will be low degree.
 * 
 */
void EpsilonTab::cleanup()
{
	HighDeg.clear();
}

void EpsilonTab::print_table()
{
	if (!hasGraph())
	{
		std::cerr << "No Graph set\n";
		return;
	}

	
	for(auto v : HighDeg)
		std::cout << v.first << " ";
	std::cout << std::endl;
	std::cout << std::endl;

	
	std::cout << "B: \n";
	for(auto v : B)
		std::cout << v.first << " ";
	std::cout << std::endl;
	std::cout << std::endl;

	std::cout << "C: \n";
	int count {0};
	for (auto c : C)
	{
		std::cout << "deg " << count << ": ";
		for (auto v : c)
			std:: cout << v.first << " ";

		count++;
		std::cout << "\n";
	}
	std::cout << std::endl;
	std::cout << std::endl;

	std::cout << "Low Degree: \n";


	graph->getDiGraph()->mapVertices([&](const Algora::Vertex *v)
	{
		if (HighDeg.count(v->getId()) == 0)
			std::cout << v->getId() << " ";
		return 1;
		
	});

	std::cout << "\n\n";
	



}

/**
 * @brief Adds a function to be called when a vertex moves to high degree.
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onVertexToHigh(void *id, const Algora::VertexMapping &vvFun)
{
	this->observableVertexUp.addObserver(id,vvFun);
}
/**
 * @brief Adds a function to be called when a vertex moves to low degree.
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onVertexToLow(void *id, const Algora::VertexMapping &vvFun)
{
	this->observableVertexDown.addObserver(id,vvFun);
}
/**
 * @brief Adds a function to be called when an arc is added to the graph
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onArcAdd(void *id, const Algora::ArcMapping &vvFun)
{
	this->observableArcAdd.addObserver(id,vvFun);
}
/**
 * @brief Adds a function to be called when an arc is removed from the graph
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onArcRemove(void *id, const Algora::ArcMapping &vvFun)
{
	this->observableArcRemove.addObserver(id,vvFun);
}
/**
 * @brief Removes a function to be called when a vertex moves to high degree.
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnVertexToHigh(void *id)
{
	this->observableVertexUp.removeObserver(id);
}
/**
 * @brief Removes a function to be called when a vertex moves to low degree.
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnVertexToLow(void *id)
{
	this->observableVertexDown.removeObserver(id);
}
/**
 * @brief Removes a function to be called when an arc is added to the graph
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnArcAdd(void *id)
{
	this->observableArcAdd.removeObserver(id);
}
/**
 * @brief Removes a function to be called when an arc is removed from the graph
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnArcRemove(void *id)
{
	this->observableArcRemove.removeObserver(id);
}
